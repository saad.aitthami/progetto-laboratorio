package App;

public class Desktop extends Pc 
{
    private double dimensioneSchermo;

    public Desktop(){}
    public Desktop(String marca, String tipo, int memoria, boolean guasto, double dimensioneSchermo)
        {
            super(marca, tipo, memoria, guasto);
            this.setMarca(marca);
            this.setTipo(tipo);
            this.setMemoria(memoria);
            this.setGuasto(guasto);
            this.dimensioneSchermo=dimensioneSchermo;
        }
    
    public Desktop(Desktop d)
        {
            super();
            this.setMarca(d.getMarca());
            this.setTipo(d.getTipo());
            this.setMemoria(d.getMemoria());
            this.setGuasto(d.isGuasto());
            this.dimensioneSchermo=d.getDimensioneSchermo();
        }

    public double getDimensioneSchermo() {return this.dimensioneSchermo;}
    public void setDimensioneSchermo(double dimensioneSchermo) {this.dimensioneSchermo = dimensioneSchermo;}


    @Override
    public Pc clone()
    {
        return new Desktop(getMarca(), getTipo(), getMemoria(), isGuasto(), getDimensioneSchermo());
    }

    @Override
    public String toString()
    {
        if(isGuasto()==false)
            {
                return "Desktop funzionante" + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + " Dimensione Schermo: " + getDimensioneSchermo() +  System.lineSeparator();
            }
        else
            {
                return "Desktop non funzionante " + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + " Dimensione Schermo: " + getDimensioneSchermo() + System.lineSeparator();
            }
    }
    
    
}
