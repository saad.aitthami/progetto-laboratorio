package App;

public class Laptop extends Pc {
    //attributi
    private int durataBatteria;

    public Laptop(){}
    public Laptop(String marca, String tipo, int memoria, boolean guasto, int durataBatteria)
        {
            super(marca,tipo,memoria,guasto);
            this.setMarca(marca);
            this.setTipo(tipo);
            this.setMemoria(memoria);
            this.setGuasto(guasto);
            this.durataBatteria=durataBatteria;
        }

    public Laptop(Laptop l)
        {
            super();
            this.setMarca(l.getMarca());
            this.setMemoria(l.getMemoria());
            this.setTipo(l.getTipo());
            this.setGuasto(l.isGuasto());
            this.durataBatteria=l.getDurataBatteria();
        }
    
    public int getDurataBatteria() {return this.durataBatteria;}
    public void setDurataBatteria(int durataBatteria) {this.durataBatteria = durataBatteria;}
    
    @Override
    public Pc clone()
        {
            return new Laptop(this.getMarca(), this.getTipo(),this.getMemoria(), this.isGuasto(),this.getDurataBatteria());
        }
    
    @Override
    public String toString()
    {
        if(isGuasto()==false)
            {
                return "Laptop funzionante" + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + " Durata Batteria: " + getDurataBatteria() +  System.lineSeparator();
            }
         else
            {
                return "Laptop non funzionante " + " Marca: " + getMarca() + " tipo: " + getTipo() + " con memoria:" + getMemoria() + "GB" + " Durata Batteria: " + getDurataBatteria() + System.lineSeparator();
            }
    }
    
}
