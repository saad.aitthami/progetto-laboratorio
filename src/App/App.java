package App;
public class App {
    public static void main(String[] args) throws Exception {
        
        Laboratorio lab= new Laboratorio();
        Pc desPc = new Desktop("Hp","Desktop",16,false,14);
        Pc desPc2 = new Desktop("Lenovo","Desktop",16,true,19);
        Pc laptopPc= new Laptop("Asus ","Laptop",19,true,80);
        lab.aggiungipc(desPc);
        lab.aggiungipc(desPc2);
        lab.aggiungipc(laptopPc);
        System.out.println(lab.toString());
        lab.remove(desPc);
        System.out.println(lab.toString());
    }
}
