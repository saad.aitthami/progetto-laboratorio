package App;

import java.io.Serializable;

public abstract class Pc implements Serializable {

    private  String Marca, Tipo;
    private  int Memoria;
    private boolean Guasto;

    //costruttore default
    public Pc()
    {
        this.Marca="";
        this.Tipo="";
        this.Memoria=0;
        this.Guasto=false;
    } 
    
    //costruttore Master
    public Pc(String Marca, String Tipo, int Memoria, boolean Guasto ) 
    {
        this.Marca=Marca;
        this.Tipo=Tipo;
        this.Memoria=Memoria;
        this.Guasto=Guasto;
    }
    
    //costruttore di copia
    public Pc(Pc pc){
        this.Marca=pc.getMarca();
        this.Tipo=pc.getTipo();
        this.Memoria=pc.getMemoria();
        this.Guasto=pc.isGuasto();
    }

    public void setMarca(String Marca){this.Marca=Marca;}
    public void setTipo(String Tipo){this.Tipo=Tipo;}
    public void setMemoria(int Memoria){this.Memoria=Memoria;}
    public void setGuasto(boolean Guasto){this.Guasto=Guasto;}

    public String getMarca(){return this.Marca;}
    public String getTipo(){return this.Tipo;}
    public int getMemoria(){return this.Memoria;}
    public boolean isGuasto() {return Guasto;}

    @Override
    public abstract Pc clone();

    @Override
    public String toString()
    {
        return " Marca "+ getMarca()+ " Tipo "+getTipo()+ " Memoria " + getMemoria() + " Guasto "+isGuasto();
    }
    
/*
    @Override
    public boolean equals(Object c){
        boolean equals = false;
        if (c == this)
        {
            equals = true;
        }
        else if (c instanceof Pc)
        {
            Pc ceo = PC o;
            return this.marca.equals(pc.getMarca())&&this.tipo.equals(pc.getTipo())&&this.memoria=pc.getmemoria && this.Guasto = pc.isGuasto();
        }

        return equals;

    }
*/
    @Override
    public boolean equals (Object o){
        if(o != null){
            if (o instanceof Pc){
                Pc pc =(Pc) o;
                return
                 this.Marca.equals(pc.getMarca()) && 
                 this.Tipo.equals (pc.getTipo()) && 
                 this.Memoria == (pc.getMemoria()) && 
                 this.Guasto == (pc.isGuasto());
            }
                
            } 
            return false;

        }
    }
    
    

