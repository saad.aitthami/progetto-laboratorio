package App;

import java.io.*;
import java.util.*;

public class Laboratorio {

    private ArrayList <Pc> pcs ;

    //costruttore default
    public Laboratorio(){
        pcs= new ArrayList<Pc>();
    }

    //costruttore master
    public Laboratorio(ArrayList<Pc> pcs)
    {
        for(Pc pc : pcs){
            this.pcs.add(pc.clone());
        }       
    }
    
    //costruttore di copia
    public Laboratorio (Laboratorio lab)
    {
        this(lab.pcs);

    }

    //Aggiungi pc

    public void aggiungipc(Pc pc){

        pcs.add(pc.clone());
    }
    /*
    public void inserire(Computer c){
        if(lista.size)
        
    }*/


    @Override
    public String toString(){

        String s ="";   
        for(Pc pc : this.pcs){
            s+= pc.toString() + System.lineSeparator(); 
            
        }
        return s;
         



    }
    public boolean remove(Pc pc)
    {      
           return pcs.remove(pc);
    }
    public void save(String fileName){
          try{ 
            final FileOutputStream f = new FileOutputStream(new File(fileName));
            final ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(pcs);
            o.close();
            f.close();
          }
          catch (IOException exception) {}


    }

    public void load(String filename){
        try {
            final FileInputStream fi = new FileInputStream(new File(filename));
            final ObjectInputStream oi = new ObjectInputStream(fi);
            // Read objects
            pcs = (ArrayList<Pc>) oi.readObject();
            
        } catch (Exception e) {}

    }



    
}
